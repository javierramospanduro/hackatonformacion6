package com.hackaton.hackatonformacion6.services;

import com.hackaton.hackatonformacion6.models.Alumno;
import com.hackaton.hackatonformacion6.models.Course;
import org.springframework.http.HttpStatus;

public class StudentResponseEntity {

    private String msg;
    private Alumno alumno;
    private HttpStatus responsehttp;

    public StudentResponseEntity() {
    }

    public StudentResponseEntity(String msg, Alumno alumno, HttpStatus responsehttp) {
        this.msg = msg;
        this.alumno = alumno;
        this.responsehttp = responsehttp;
    }

    public String getMsg() {
        return this.msg;
    }

    public Alumno getAlumno() {
        return this.alumno;
    }

    public HttpStatus getResponsehttp() {
        return this.responsehttp;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public void setResponsehttp(HttpStatus responsehttp) {
        this.responsehttp = responsehttp;
    }

    @Override
    public String toString() {
        return "StudentResponseEntity{" +
                "msg='" + msg + '\'' +
                ", alumno=" + alumno +
                ", responsehttp=" + responsehttp +
                '}';
    }
}
