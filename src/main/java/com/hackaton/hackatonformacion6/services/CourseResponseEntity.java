package com.hackaton.hackatonformacion6.services;

import com.hackaton.hackatonformacion6.models.Course;
import org.springframework.http.HttpStatus;

public class CourseResponseEntity {

    private String msg;
    private Course course;
    private HttpStatus responsehttp;

    public CourseResponseEntity() {
    }

    public CourseResponseEntity(String msg, Course course, HttpStatus responsehttp) {
        this.msg = msg;
        this.course = course;
        this.responsehttp = responsehttp;
    }

    public String getMsg() {
        return this.msg;
    }

    public Course getCourse() {
        return this.course;
    }

    public HttpStatus getResponsehttp() {
        return this.responsehttp;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "CourseResponseEntity{" +
                "msg='" + msg + '\'' +
                ", course=" + course +
                ", responsehttp=" + responsehttp +
                '}';
    }

    public void setResponsehttp(HttpStatus responsehttp) {
        this.responsehttp = responsehttp;
    }
}
