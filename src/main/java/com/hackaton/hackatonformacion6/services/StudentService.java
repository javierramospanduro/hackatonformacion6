package com.hackaton.hackatonformacion6.services;

import com.hackaton.hackatonformacion6.Hackatonformacion6Application;
import com.hackaton.hackatonformacion6.models.Alumno;
import com.hackaton.hackatonformacion6.models.DataItineraries;
import com.hackaton.hackatonformacion6.models.DataStudent;
import com.hackaton.hackatonformacion6.models.Itinerario;
import com.hackaton.hackatonformacion6.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    StudentRepository studentRepository;

    public DataStudent findAll(){
        System.out.println("en findAll de studentservice");
        return this.studentRepository.findAll();
    }

    public boolean delete (String id) {
        System.out.println("delete en student Service");
        boolean result = false;
        for (Alumno alumno : Hackatonformacion6Application.studentmodel.getAlumnos()){
            if (alumno.getId().equals(id)) {
                System.out.println("Alumno encontrado");
                result = true;
                this.studentRepository.delete(alumno);
                System.out.println("en studentservice a la vuelta de studenrespository ");
                return result;
            }
        }
        System.out.println("result en delete de studentservice " + result);
        return result;
    }

    public StudentResponseEntity add(Alumno alumno) {
        System.out.println("addalumno en studentservice");
        StudentResponseEntity result = new StudentResponseEntity();
        for (Alumno student : Hackatonformacion6Application.studentmodel.getAlumnos()){
            if (alumno.getId().equals(student.getId())) {
                System.out.println("Alumno encontrado en add");
                result.setMsg("Alumno ya existente");
                result.setResponsehttp(HttpStatus.BAD_REQUEST);
                System.out.println("en studentservice a la vuelta de studenrespository ");
                return result;
            }
        }
        this.studentRepository.newstudent(alumno);
        result.setMsg("Alumno dado de alta correctamente");
        result.setResponsehttp(HttpStatus.OK);
        System.out.println("en studentservice después de añadir alumno");
        return result;
    }

    public StudentResponseEntity update (Alumno alumno) {
        System.out.println("en update de student service");
        return this.studentRepository.update(alumno);
    }
}
