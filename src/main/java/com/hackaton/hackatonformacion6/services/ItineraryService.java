package com.hackaton.hackatonformacion6.services;

import com.hackaton.hackatonformacion6.models.DataItineraries;
import com.hackaton.hackatonformacion6.models.Itinerario;
import com.hackaton.hackatonformacion6.repository.ItineraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ItineraryService {

    @Autowired
    ItineraryRepository itineraryRepository;

    public ItineraryResponseEntity add(Itinerario itinerario) {
        System.out.println("En itinerayService");

        ItineraryResponseEntity result = new ItineraryResponseEntity();

        System.out.println("Alta de itinerario");

        this.itineraryRepository.save(itinerario);

        result.setItinerario(itinerario);
        result.setMsg("Alta de itinerario");
        result.setResponsehttp(HttpStatus.OK);

        return result;
    }

    public DataItineraries findall() {
        System.out.println("findall en itineraryservice");
        return this.itineraryRepository.findAll();
    }

    public Optional<Itinerario> getdById(String id) {
        System.out.println("findByID en PurchaseService ");

        return this.itineraryRepository.findById(id);
    }
}
