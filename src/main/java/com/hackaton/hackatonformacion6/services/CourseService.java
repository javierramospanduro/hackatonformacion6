package com.hackaton.hackatonformacion6.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.hackaton.hackatonformacion6.Hackatonformacion6Application;
import com.hackaton.hackatonformacion6.models.Course;
import com.hackaton.hackatonformacion6.models.DataCourses;
import com.hackaton.hackatonformacion6.models.DataItineraries;
import com.hackaton.hackatonformacion6.models.Itinerario;
import com.hackaton.hackatonformacion6.repository.CourseRepository;

@Service
public class CourseService {
    @Autowired
    CourseRepository courseRepository;


    public CourseResponseEntity add(String iditin, Course course) {
        System.out.println("En CourseService");

        System.out.println("busco el itinerario " + course);

        Optional<Itinerario> itinerariotofind = this.findById(iditin);

        if (itinerariotofind.isPresent() == true){
            System.out.println("itinerario encontrado en add de courseservice");

            List<Course> listacursos = itinerariotofind.get().getCourses();

            System.out.println("cargo los cursos del itinerario " + itinerariotofind.get().getCourses());

            if (listacursos == null ){
                listacursos = new ArrayList<>();
            }

            listacursos.add(course);
            itinerariotofind.get().setCourses(listacursos);

            System.out.println("añado curso a itinerario encontrado " + course);

            this.courseRepository.save(itinerariotofind.get());

        } else {
            System.out.println("itinerario no encontrado");
            CourseResponseEntity result = new CourseResponseEntity();
            result.setCourse(null);
            result.setMsg("Itinerario no encontrado");
            result.setResponsehttp(HttpStatus.NOT_FOUND);
            System.out.println(result);
            return result;
        }

        CourseResponseEntity result = new CourseResponseEntity();
        System.out.println("Alta de curso");
        result.setCourse(course);
        result.setMsg("Curso dado de alta correctamente");
        result.setResponsehttp(HttpStatus.OK);

        return result;
    }

    public Optional<Itinerario> findById(String itinerarioId) {
        System.out.println("findByID en courseService");

        Optional<Itinerario> result = Optional.empty();

        DataItineraries itinerarioinList = new DataItineraries();
        List<Itinerario> listaitinerarios = itinerarioinList.getListaitinerarios();

        for (Itinerario itinerario : Hackatonformacion6Application.dataitinerarymodel.getListaitinerarios()) {
            if (itinerario.getId().equals(itinerarioId)) {
                System.out.println("Itinerario encontrado");
                result = Optional.of(itinerario);
            }
        }
        return result;
    }
    public DataCourses findall() {
        System.out.println("findall en cursos");
        return this.courseRepository.findAll();
    }

    public Optional<DataCourses> finById(String idit, String nameco) {
        System.out.println("findById en CourseService ");

        return this.courseRepository.finById(idit, nameco);
    }
}
