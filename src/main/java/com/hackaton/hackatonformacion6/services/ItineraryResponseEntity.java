package com.hackaton.hackatonformacion6.services;

import com.hackaton.hackatonformacion6.models.Itinerario;
import org.springframework.http.HttpStatus;

public class ItineraryResponseEntity {

    private String msg;
    private Itinerario itinerario;
    private HttpStatus responsehttp;

    public ItineraryResponseEntity() {
    }

    public ItineraryResponseEntity(String msg, Itinerario itinerario, HttpStatus responsehttp) {
        this.msg = msg;
        this.itinerario = itinerario;
        this.responsehttp = responsehttp;
    }

    public String getMsg() {
        return this.msg;
    }

    public Itinerario getItinerario() {
        return this.itinerario;
    }

    public HttpStatus getResponsehttp() {
        return this.responsehttp;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setItinerario(Itinerario itinerario) {
        this.itinerario = itinerario;
    }

    @Override
    public String toString() {
        return "ItineraryResponseEntity{" +
                "msg='" + msg + '\'' +
                ", itinerario=" + itinerario +
                ", responsehttp=" + responsehttp +
                '}';
    }

    public void setResponsehttp(HttpStatus responsehttp) {
        this.responsehttp = responsehttp;
    }
}
