package com.hackaton.hackatonformacion6.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hackaton.hackatonformacion6.models.Course;
import com.hackaton.hackatonformacion6.models.DataCourses;
import com.hackaton.hackatonformacion6.services.CourseResponseEntity;
import com.hackaton.hackatonformacion6.services.CourseService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/formation")
public class CourseController {
    @Autowired
    CourseService courseService;

    @PostMapping("/itineraries/{id}/courses")
    public ResponseEntity<String> addCourse(@PathVariable String id, @RequestBody Course courseentrada) {
        System.out.println("addCourse");
        System.out.println("La id del curso es " + courseentrada.getId());
        System.out.println("El nombre del curso es " + courseentrada.getName());
        System.out.println("La descripción del curso es " + courseentrada.getDescription());
        System.out.println("Los ediciones del curso son " + courseentrada.getEditions());
        System.out.println("La duración del curso es " + courseentrada.getDuration());

        CourseResponseEntity courseResponseEntity = this.courseService.add(id, courseentrada);

        return new ResponseEntity<>(
            courseResponseEntity.getMsg(),
            courseResponseEntity.getResponsehttp()
        );
    }
    @GetMapping("/courses")
    public ResponseEntity<DataCourses> getCourses() {
        System.out.println("en CourseController - getICourses");

        return new ResponseEntity<>(
                this.courseService.findall() , HttpStatus.OK);
    }

 

    @GetMapping("/itineraries/{idit}/courses/{namecou}")
    public ResponseEntity<Object> getCourseById(@PathVariable String idit, @PathVariable String namecou) {
        System.out.println("getCourseByid");
        System.out.println("La id del itinerario a buscar es " + idit);
        System.out.println("La id del curso a buscar es " + namecou);

        Optional<DataCourses> result = this.courseService.finById(idit, namecou);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Curso no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }

}