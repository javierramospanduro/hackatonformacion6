package com.hackaton.hackatonformacion6.controllers;

import com.hackaton.hackatonformacion6.Hackatonformacion6Application;
import com.hackaton.hackatonformacion6.models.DataItineraries;
import com.hackaton.hackatonformacion6.models.Itinerario;
import com.hackaton.hackatonformacion6.services.ItineraryResponseEntity;
import com.hackaton.hackatonformacion6.services.ItineraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/formation")
public class ItineraryController {

    @Autowired
    ItineraryService itineraryService;

    @GetMapping("/itineraries")
    public ResponseEntity<DataItineraries> getItinerarios() {
        System.out.println("en ItienarioController - getItinerarios");
        return new ResponseEntity<>(
                this.itineraryService.findall(), HttpStatus.OK
        );
    }

    @PostMapping("/itineraries")
    public ResponseEntity<Itinerario> addItinerario(@RequestBody Itinerario itinerarioentrada) {
        System.out.println("addItinerario");
        System.out.println("La id del itinerario es " + itinerarioentrada.getId());
        System.out.println("El nombre del itinerario es " + itinerarioentrada.getName());
        System.out.println("La descripción del itinerario es " + itinerarioentrada.getDescription());
        System.out.println("Los cursos  del itinerario son " + itinerarioentrada.getCourses());

        ItineraryResponseEntity itineraryServiceResponse = this.itineraryService.add(itinerarioentrada);

        return new ResponseEntity<>(
                itineraryServiceResponse.getItinerario(),
                itineraryServiceResponse.getResponsehttp()
        );

    }
}
