package com.hackaton.hackatonformacion6.controllers;

import com.hackaton.hackatonformacion6.models.Alumno;
import com.hackaton.hackatonformacion6.models.DataStudent;
import com.hackaton.hackatonformacion6.services.ItineraryResponseEntity;
import com.hackaton.hackatonformacion6.services.StudentResponseEntity;
import com.hackaton.hackatonformacion6.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/formation")
public class StudentController {
    @Autowired
    StudentService studentService;

    @GetMapping("/students")
    public ResponseEntity<DataStudent> listalumnos() {
        return new ResponseEntity<>(
                this.studentService.findAll(), HttpStatus.OK
        );
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable String id) {
        System.out.println("deleteStudent en student controller");
        System.out.println("La id del student a borrar es " + id);
        boolean deleteStudent = this.studentService.delete(id);
        System.out.println("en studentcontroller a la vuelta de student service " + deleteStudent);
        return new ResponseEntity<>(
                deleteStudent ? "Alumno borrado" : "Alumno no encontrado",
                deleteStudent ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/students")
    public ResponseEntity<String> addalumno(@RequestBody Alumno alumno) {
        System.out.println(" en addalumno de studentcontroller");
        StudentResponseEntity studentResponseEntity = this.studentService.add(alumno);
        return new ResponseEntity<>(
          studentResponseEntity.getMsg(),studentResponseEntity.getResponsehttp()
        );
    }

    @PutMapping("/students")
    public ResponseEntity<String> updatealumno (@RequestBody Alumno alumno) {
        System.out.println("en update alumno de student controller" );
        StudentResponseEntity studentResponseEntity = this.studentService.update(alumno);
        return new ResponseEntity<>(studentResponseEntity.getMsg(),studentResponseEntity.getResponsehttp());
    }
}
