package com.hackaton.hackatonformacion6.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.hackaton.hackatonformacion6.Hackatonformacion6Application;
import com.hackaton.hackatonformacion6.models.DataItineraries;
import com.hackaton.hackatonformacion6.models.Itinerario;

@Repository
public class ItineraryRepository {


    public Itinerario save (Itinerario itinerario){
        System.out.println("En ItieneraryRepository");
        System.out.println("save de itinerario");
        Hackatonformacion6Application.dataitinerarymodel.add(itinerario);
        return itinerario;
    }

    public DataItineraries findAll() {
        System.out.println("FindAll en itineraryRepository");
        System.out.println("--salida itinerary repository " + Hackatonformacion6Application.dataitinerarymodel);
        return Hackatonformacion6Application.dataitinerarymodel;
    }

    public Optional<Itinerario> findById(String id) {
        System.out.println("FindById en ItinerarioRepository");

        Optional<Itinerario> result = Optional.empty();

        for(Itinerario itinerarioInList : Hackatonformacion6Application.dataitinerarymodel.getListaitinerarios()) {
            if (itinerarioInList.getId().equals(id)) {
                System.out.println("Itinerario encontrado");
                result = Optional.of(itinerarioInList);
            }
        }

        return result;
    }

}
