package com.hackaton.hackatonformacion6.repository;

import com.hackaton.hackatonformacion6.Hackatonformacion6Application;
import com.hackaton.hackatonformacion6.models.Alumno;
import com.hackaton.hackatonformacion6.models.DataStudent;
import com.hackaton.hackatonformacion6.services.StudentResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentRepository {

    public DataStudent findAll() {
        System.out.println("en findAll de studentRepository");
        System.out.println(Hackatonformacion6Application.studentmodel);
        return Hackatonformacion6Application.studentmodel;
    }
    public void delete(Alumno alumno) {
        System.out.println("delete en studentrepository " + alumno);
        System.out.println("lista de alumnos antes de borrar " +
                Hackatonformacion6Application.studentmodel.getAlumnos());
        Hackatonformacion6Application.studentmodel.getAlumnos().remove(alumno);
    }

    public Alumno newstudent (Alumno alumno) {
        System.out.println("newstudent en studentrepository " + alumno);
        Hackatonformacion6Application.studentmodel.getAlumnos().add(alumno);
        return alumno;
    }

    public StudentResponseEntity update (Alumno alumno) {
        System.out.println("update alumno en student repository");
        StudentResponseEntity result = new StudentResponseEntity();
        for (Alumno student : Hackatonformacion6Application.studentmodel.getAlumnos()){
            if (alumno.getId().equals(student.getId())) {
                System.out.println("Alumno encontrado en update");
                student.setItinerario(alumno.getItinerario());
                result.setMsg("Alumno actualizado correctamente");
                result.setResponsehttp(HttpStatus.OK);
                return result;
            }
        }
        result.setMsg("Alumno no encontrado");
        result.setResponsehttp(HttpStatus.NOT_FOUND);
        return result;
    }
}
