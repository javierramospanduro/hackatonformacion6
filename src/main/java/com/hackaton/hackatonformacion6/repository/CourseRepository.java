package com.hackaton.hackatonformacion6.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hackaton.hackatonformacion6.Hackatonformacion6Application;
import com.hackaton.hackatonformacion6.models.Course;
import com.hackaton.hackatonformacion6.models.DataCourses;
import com.hackaton.hackatonformacion6.models.Itinerario;

@Repository
public class CourseRepository {

    @Autowired
    ItineraryRepository itineraryRepository;

    public DataCourses findAll() {
        System.out.println("FindAll en CoursesRepository");
        return Hackatonformacion6Application.dataCourses;
    }

    public Optional<DataCourses> finById(String itinerarioId, String courseName) {
        System.out.println("findById en Repository");

        Optional<Itinerario> result = itineraryRepository.findById(itinerarioId);

        if (result.isPresent()) {
            for(Course course : result.get().getCourses()) {
                if (course.getName().equals(courseName)) {
                    DataCourses rc = new DataCourses();
                    rc.setListacourses(new ArrayList<>());
                    rc.getListacourses().add(course);
                    return Optional.of(rc);
                }
            }
        }
        return Optional.empty();
    }
    public Itinerario save (Itinerario itinerario){
        System.out.println("En CourseRepository");
        System.out.println("save de course  " + itinerario);

        List<Itinerario> l = Hackatonformacion6Application.dataitinerarymodel.getListaitinerarios();
        for (int i = 0; i < l.size(); i++) {
            if (l.get(i).getId().equals(itinerario.getId())){
                l.get(i).setCourses(itinerario.getCourses());
            }
        }
        System.out.println("después del FOR " + l);
        return itinerario;
    }

}
