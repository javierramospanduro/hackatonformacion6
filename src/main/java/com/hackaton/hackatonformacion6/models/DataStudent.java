package com.hackaton.hackatonformacion6.models;

import java.util.List;

public class DataStudent {
    List<Alumno> alumnos;

    public DataStudent() {
    }

    public DataStudent(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public List<Alumno> getAlumnos() {
        return this.alumnos;
    }

    public void setAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    @Override
    public String toString() {
        return "DataStudent{" +
                "alumnos=" + alumnos +
                '}';
    }
}
