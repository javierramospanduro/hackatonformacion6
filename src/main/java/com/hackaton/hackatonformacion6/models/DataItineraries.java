package com.hackaton.hackatonformacion6.models;

import java.util.List;

public class DataItineraries  {
    List<Itinerario> listaitinerarios;

    public DataItineraries() {
    }

    public DataItineraries(List<Itinerario> listaitinerarios) {
        this.listaitinerarios = listaitinerarios;
    }

    public List<Itinerario> getListaitinerarios() {
        return this.listaitinerarios;
    }

    public void setListaitinerarios(List<Itinerario> listaitinerarios) {
        this.listaitinerarios = listaitinerarios;
    }

    public void add(Itinerario itinerario) {
        listaitinerarios.add(itinerario);
    }

    @Override
    public String toString() {
        return "DataItineraries{" +
                "listaitinerarios=" + listaitinerarios +
                '}';
    }
}
