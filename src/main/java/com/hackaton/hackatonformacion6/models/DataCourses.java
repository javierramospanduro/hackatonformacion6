package com.hackaton.hackatonformacion6.models;

import java.util.List;

public class DataCourses {

    List<Course> listacourses;

    public DataCourses() {
    }

    public DataCourses(List<Course> listacourses) {
        this.listacourses = listacourses;
    }

    public List<Course> getListacourses() {
        return this.listacourses;
    }

    public void setListacourses(List<Course> listacourses) {
        this.listacourses = listacourses;
    }

    public void add(Course course) {
        listacourses.add(course);
    }

    @Override
    public String toString() {
        return "DataCourses{" +
                "listacourses=" + listacourses +
                '}';
    }
}