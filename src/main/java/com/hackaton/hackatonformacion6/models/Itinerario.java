package com.hackaton.hackatonformacion6.models;

import java.util.List;

public class Itinerario {
    String id;
    String name;
    String description;
    List<Course> courses;

    public Itinerario() {
    }

    public Itinerario(String id, String name, String description, List<Course> courses) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.courses = courses;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public List<Course> getCourses() {
        return this.courses;
    }

    @Override
    public String toString() {
        return "Itinerario{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", courses=" + courses +
                '}';
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
