package com.hackaton.hackatonformacion6.models;

import java.util.List;

public class DataAlumnos {

    List<Alumno> listaAlumnos;

    public DataAlumnos() {
    }

    public DataAlumnos(List<Alumno> listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
    }

    public List<Alumno> getListaAlumnos() {
        return this.listaAlumnos;
    }

    public void setListaAlumnos(List<Alumno> listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
    }
}
