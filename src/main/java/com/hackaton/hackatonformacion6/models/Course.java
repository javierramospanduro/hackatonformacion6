package com.hackaton.hackatonformacion6.models;

import java.util.List;

public class Course {
    String id;
    String name;
    String description;
    List<Edition> editions;
    Integer duration;

    public Course() {
    }

    public Course(String id, String name, String description, List<Edition> editions, Integer duration) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.editions = editions;
        this.duration = duration;
    }

    public String getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", editions=" + editions +
                ", duration=" + duration +
                '}';
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public List<Edition> getEditions() {
        return this.editions;
    }

    public Integer getDuration() {
        return this.duration;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEditions(List<Edition> editions) {
        this.editions = editions;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
