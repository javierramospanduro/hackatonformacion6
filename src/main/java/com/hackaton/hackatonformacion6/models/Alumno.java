package com.hackaton.hackatonformacion6.models;

public class Alumno {
    String id;
    String name;
    Itinerario itinerario;

    public Alumno() {
    }

    public Alumno(String id, String name, Itinerario itinerario) {
        this.id = id;
        this.name = name;
        this.itinerario = itinerario;
    }

    public String getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", itinerario=" + itinerario +
                '}';
    }

    public String getName() {
        return this.name;
    }

    public Itinerario getItinerario() {
        return this.itinerario;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setItinerario(Itinerario itinerario) {
        this.itinerario = itinerario;
    }
}
