package com.hackaton.hackatonformacion6.models;

import java.util.Date;
import java.util.List;

public class Edition {
    String id;
    String name;
    String description;
    Date startDate;
    Date endDate;
    List<Alumno> alumnos;

    public Edition() {
    }

    public Edition(String id, String name, String description, Date startDate, Date endDate, List<Alumno> alumnos) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.alumnos = alumnos;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public List<Alumno> getAlumnos() {
        return this.alumnos;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Edition{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", alumnos=" + alumnos +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }
}
