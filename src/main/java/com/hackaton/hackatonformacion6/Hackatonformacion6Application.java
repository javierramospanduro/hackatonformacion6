package com.hackaton.hackatonformacion6;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hackaton.hackatonformacion6.models.Alumno;
import com.hackaton.hackatonformacion6.models.Course;
import com.hackaton.hackatonformacion6.models.DataCourses;
import com.hackaton.hackatonformacion6.models.DataItineraries;
import com.hackaton.hackatonformacion6.models.DataStudent;
import com.hackaton.hackatonformacion6.models.Itinerario;

@SpringBootApplication
public class Hackatonformacion6Application {

	public static DataItineraries dataitinerarymodel;
	public static DataCourses dataCourses;
	public static DataStudent studentmodel;

	public static void main(String[] args) {

		System.out.println("empezamos hackaton");
	SpringApplication.run(Hackatonformacion6Application.class, args);
		Hackatonformacion6Application.dataitinerarymodel =
						Hackatonformacion6Application.getitinerarioTestData();
			Hackatonformacion6Application.dataCourses =
					Hackatonformacion6Application.getCourseTestData();
			Hackatonformacion6Application.studentmodel =
					Hackatonformacion6Application.getstudentTestData();
		Hackatonformacion6Application.dataCourses =
				Hackatonformacion6Application.getCourseTestData();

	}

	private static DataItineraries getitinerarioTestData() {

		System.out.println("En getitinerarioTestData");
		ArrayList<Itinerario> itinerarios= new ArrayList<>();

		itinerarios.add(
				new Itinerario("1","Back","BackEnd",null)
		);
		itinerarios.add(
				new Itinerario("2","Front","FrontEnd",null)
		);
		itinerarios.add(
				new Itinerario("3","Seguridad","Seguridad",null)
		);
		DataItineraries dataItineraries = new DataItineraries(itinerarios);
		System.out.println("añadimos itinerarios");

		return dataItineraries;
	}

	private static DataCourses getCourseTestData() {

		//ArrayList<Course> cursos = new ArrayList<>();


		ArrayList<Course> courses = new ArrayList<>();
		courses.add(
				new Course("1","Practitioner Back End","Descripción pratitoner BackEnd",null,64)
		);
		courses.add(
				new Course("2","Practitioner Front End","Practitioner Front End",null,64)
		);
		courses.add(
				new Course("3","Fundamentos de Java","Fundamentos de Java",null,20)
		);
		DataCourses dataCourses = new DataCourses(courses);
		System.out.println("añadimos cursos");
		return dataCourses;
	}

	private static DataStudent getstudentTestData() {
		ArrayList<Alumno> alumnos = new ArrayList<>();
		alumnos.add(
				new Alumno("1","Fernando Garcia", null )
		);
		alumnos.add(
				new Alumno("2","María Sanchez", null )
		);
		alumnos.add(
				new Alumno("3","Jose Perez", null )
		);
		alumnos.add(
				new Alumno("4","Jose Fernández", null )
		);
		alumnos.add(
				new Alumno("5","Josefa Ruiz", null )
		);
		DataStudent dataStudent = new DataStudent(alumnos);
		return dataStudent;
	}
}
